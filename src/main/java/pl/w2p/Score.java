package pl.w2p;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Score {
    public int wins, losses, ties;

    @Override
    public String toString() {
        return "Score{" +
                "wins=" + wins +
                ", losses=" + losses +
                ", ties=" + ties +
                '}';
    }
}

